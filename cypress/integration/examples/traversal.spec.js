/// <reference types="cypress" />

context('Traversal', () => {
  beforeEach(() => {
    cy.visit('https://example.cypress.io/commands/traversal')
  })

const dataItems = ['Data', 'Data', 'Data']
  dataItems.forEach((dataItem) => {

      it('try ' + dataItem, () => {
        // https://on.cypress.io/children
        cy.get('.traversal-breadcrumb')
          .children('.active')
          .should('contain', dataItem)
      })
  })
})
